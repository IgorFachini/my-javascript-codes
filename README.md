# my-javascript-codes
A repository to place all my JavaScript codes :)  Made with love with JS&nbsp;&nbsp;:heart:

### Links for each project

* [Number Conversion](https://williamparlow.github.io/my-javascript-codes/NumberConvertion/)
* [Canvas Graph](https://williamparlow.github.io/my-javascript-codes/CanvasGraph/)
* [Search Box Prototype](https://williamparlow.github.io/my-javascript-codes/searchbox/)
* [A Simple Simple Paint](https://williamparlow.github.io/my-javascript-codes/a%20simple%20simple%20paint/)
* [My Worm Game](https://williamparlow.github.io/my-javascript-codes/WormGame/)

#### _*OBS: Use google chrome for the best user experience. Developed to use all new JS, HTML and CSS especifications*_
